<?php
define('API_KEY', 'test_neocrm');
require __DIR__ . '/libs/core.php';

function dump($str)
{
    echo '<pre>';
    print_r($str);
    echo '</pre>';
}

$data = $_POST;
$core = new Core();

switch ($data['type']) {
    case 'add':
        if ($core->setData($data)) {
            die(json_encode(['status' => true]));
        };
        break;
    case 'list':
        $lastID = $data['last'] ? $data['last'] : 0;
        $res = $core->getData($lastID);
        die(json_encode(['status' => true, 'message' => $res]));
        break;
}
die(json_encode(['status' => false]));