import $ from "jquery";
import Chance from "chance";
import { validateAll } from "indicative";

// http://chancejs.com/usage/node.html
const chance = new Chance();

// https://indicative.adonisjs.com/
const rules = {
  number: "required",
  name: "required",
  direction: "required"
};

const dataAutoName = [
  "Aston Martin",
  "Acura",
  "Alfa Romeo",
  "Audi",
  "Bentley",
  "BMW",
  "Bugatti",
  "Buick",
  "Cadillac",
  "Chrysler",
  "Daihatsu",
  "Datsun",
  "DS"
];
const dataDirection = ["left", "right", "forward", "backwards"];

class core {
  constructor() {
    this.items = {};
  }

  start() {
    this.setData();
    this.update();
  }

  update(time = 5000) {
    setInterval(() => {
      let lastID = $("table tr:first-child")
        ? $("table tr:first-child").data("key")
        : 0;
      $.ajax({
        type: "post",
        url: "/api.php",
        data: { type: "list", last: lastID },
        dataType: "json",
        success: res => {
          if (res.status && res.message) {
            this.setItemsDOM(res.message);
            this.clearItemsDOM();
          }
        }
      });
    }, time);
  }

  setItemsDOM(list) {
    if (!list || !list.length) return;
    for (const item of list) {
      let itemDom = $(`table tr[data-key=${item.id}]`);
      this.items[item.id] = item;
      !itemDom.length ? $("table").prepend(this.getRow(item)) : null;
    }
  }

  clearItemsDOM(minutes = 2) {
    let day = new Date();
    let keys = Object.keys(this.items);
    day.setTime(day.getTime() - minutes * 60 * 1000); // минус 2 мин. 1мин=60000 мил.сек

    for (const key of keys) {
      let itemDate = new Date(this.items[key].date);

      if (day.getTime() < itemDate.getTime()) {
        $(`table tr[data-key=${key}]`).remove();
        delete this.items[key];
      }
    }
  }

  getRow(item) {
    return `<tr data-key='${item.id}'>
    <td class="row id">${item.id})</td>
    <td class="row date">${item.date}</td>
    <td class="row direction">${item.direction}</td>
    <td class="row name">${item.name}</td>
    <td class="row number">${item.number}</td>
    </tr>`;
  }

  getFaker() {
    return {
      type: "add",
      direction:
        dataDirection[
          chance.integer({ min: 0, max: dataDirection.length - 1 })
        ],
      name:
        dataAutoName[chance.integer({ min: 0, max: dataAutoName.length - 1 })],
      number: chance.integer({ min: 0, max: 100 })
    };
  }

  setData(time = 7000) {
    setInterval(() => {
      $.ajax({
        url: "/api.php",
        data: this.getFaker(),
        type: "post",
        dataType: "json"
      });
    }, time);
  }

  submit(idName) {
    let form = $(`#${idName}`);
    form.on("submit", e => {
      e.preventDefault();

      const url = $(form).attr("action");
      const data = form.serializeArray().reduce(
        (arr, item) => {
          arr[item.name] = item.value;
          return arr;
        },
        { auto: false, type: "add" }
      );

      $(form)
        .find(".error")
        .removeClass("error");

      validateAll(data, rules)
        .then(() => {
          $.ajax({
            url,
            data,
            type: "post",
            dataType: "json",
            success: ({ status }) => {
              if (status) $(".message").html("Путь успшно добавлен");
              else $(".message").html("Возникла ошибка");
            }
          });
        })
        .catch(errors => {
          for (const err of errors) {
            $(form)
              .find(`[name=${err.field}]`)
              .parent()
              .addClass("error");
          }
        });
    });
  }
}

window.core = new core();
