<?
if (!defined('API_KEY')) return die('Error');

class DataBase
{
    protected $servername = 'db';
    protected $username = 'app';
    protected $password = 'app';
    protected $database = 'app';

    protected $db;

    public function __construct()
    {
        if (!$this->db) $this->connect();
    }

    protected function connect()
    {
        $this->db = mysqli_connect($this->servername, $this->username, $this->password, $this->database);
        if (!$this->db) exit('Connection failed: ' . mysqli_connect_error() . PHP_EOL);
    }

    public function getDB()
    {
        return $this->db;
    }

    public function add($table, $values)
    {
        if (!$table or !$values) return;
        if (!$this->db) $this->connect();

        $sqlColumns = implode('`,`', array_keys($values));
        $sqlValues = implode("','", $values);

        $query = "INSERT INTO `$table` (`$sqlColumns`) VALUES ('$sqlValues')";
        $this->db->query($query);

        return $this->db->insert_id;
    }

    public function close()
    {
        if ($this->db) mysqli_close($this->db);
    }
}