<?
if (!defined('API_KEY')) return die('Error');

require __DIR__ . '/db.php';

class Core extends DataBase
{
    protected $table = 'car_number';
    protected $columns = ['number', 'name', 'direction', 'auto'];

    public function getData($lasID = 0)
    {
        $where = '';
        if ($lasID) $where = " WHERE `id`>'$lasID'";

        $query = "SELECT * FROM `$this->table`$where ORDER BY date ASC";
        $result = $this->db->query($query);

        $data = [];
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }

    public function setData($params)
    {
        $data = [];
        foreach ($this->columns as $key) {
            if ($params[$key]) $data[$key] = $params[$key];
        }

        $this->add($this->table, $data);
        $this->clearDataTime();
        return true;
    }

    public function clearDataTime()
    {
        $date = date("Y-m-d H:i:s", strtotime('-2 minutes'));
        $query = "DELETE FROM `$this->table` WHERE `date`<'$date' AND `auto`=true";
        $this->db->query($query);
    }
}