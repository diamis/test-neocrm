const path = require("path");
const TerserPlugin = require("terser-webpack-plugin");

module.exports = {
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "src"),
    filename: "index.min.js"
  },
  watch: true,
  optimization: {
    minimizer: [new TerserPlugin()]
  }
};
