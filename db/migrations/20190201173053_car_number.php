<?php


use Phinx\Migration\AbstractMigration;

class CarNumber extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('car_number');
        $table->addColumn('number', 'integer')       // номер транспорта
            ->addColumn('name', 'string')            // наименование транспорта
            ->addColumn('date', 'timestamp', ['default' => 'CURRENT_TIMESTAMP']) // дата-время фиксации события
            ->addColumn('direction', 'string')       // направление
            ->addColumn('auto', 'boolean', ['default' => true])
            ->addIndex(['date'], ['unique' => true]) // маркер ручного ввода
            ->create();

    }
}
